package test

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
)

const baseUrlEthNode = "https://intensive-wiser-paper.ethereum-goerli.discover.quiknode.pro/3bdd0d8b2becb615c4821aca9e0e4425647bf4bd"

func TestConnectEthNet(t *testing.T) {
	client, err := ethclient.Dial(baseUrlEthNode)

	if err != nil {
		log.Fatalf("Oops! There was a problem: %v", err)
	}

	fmt.Println("Success! you are connected to the Ethereum Network")

	header, err := client.HeaderByNumber(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println(header.Number.String())
	}
}

const formatWrite = `
{
	private key: %s,
	public key: %s,
	eth address: %s
}
`

func TestGenerateAddressEth(t *testing.T) {
	privateKey, err := crypto.GenerateKey()

	if err != nil {
		log.Fatal(err)
	}

	privateKeyBytes := crypto.FromECDSA(privateKey)

	fmt.Println("SAVE BUT DO NOT SHARE THIS (Private Key): ", hexutil.Encode(privateKeyBytes))

	publicKey := privateKey.Public()

	publicKeyECDSA, match := publicKey.(*ecdsa.PublicKey)

	if !match {
		log.Fatal("cannot assert type: publicKey is not of type *ecdsa.PublicKey")
	}

	publicKeyBytes := crypto.FromECDSAPub(publicKeyECDSA)
	fmt.Println("public key: ", hexutil.Encode(publicKeyBytes))

	ethAddress := crypto.PubkeyToAddress(*publicKeyECDSA).Hex()
	fmt.Println("Ethereum Address: ", ethAddress)

	resultWrite := fmt.Sprintf(formatWrite, hexutil.Encode(privateKeyBytes), hexutil.Encode(publicKeyBytes), ethAddress)

	err = os.WriteFile("example.txt", []byte(resultWrite), 0644)

	if err != nil {
		log.Fatal(err)
	}
}
